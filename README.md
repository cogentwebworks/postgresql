## small Postgresql container based on latest Alpine with workings6-overlay process and socklog-overlay.

[![Docker Pulls](https://img.shields.io/docker/pulls/cogentwebs/postgresql.svg)](https://hub.docker.com/r/cogentwebs/postgresql/)
[![Docker Stars](https://img.shields.io/docker/stars/cogentwebs/postgresql.svg)](https://hub.docker.com/r/cogentwebs/postgresql/)
[![Docker Build](https://img.shields.io/docker/automated/cogentwebs/postgresql.svg)](https://hub.docker.com/r/cogentwebs/postgresql/)
[![Docker Build Status](https://img.shields.io/docker/build/cogentwebs/postgresql.svg)](https://hub.docker.com/r/cogentwebs/postgresql/)

This is a small Postgresql container but still have a working s6-overlay process and  socklog-overlay . This is the base image for general small containers.
